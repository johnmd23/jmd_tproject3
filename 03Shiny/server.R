#server
require("jsonlite")
require("RCurl")
require(ggplot2)
require(dplyr)
require(shiny)
require(shinydashboard)
require(leaflet)
require(DT)

shinyServer(function(input, output) {
  
  maxval <- reactive({input$maxval})
  
  #tab1
  df1 <- eventReactive(input$clicks1, {data.frame(fromJSON(getURL(URLencode(gsub("\n", " ", 'oraclerest.cs.utexas.edu:5001/rest/native/?query=
            "select * from emissions where co2_emissions > "p1" and year = \'2012\' and country != \'World\';"')), httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER='cs329e_mas8296', PASS='orcl_mas8296',  MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON', p1=maxval()), verbose = TRUE)))
  })
   output$Plot1 <- renderPlot({             
    plot <- ggplot() + 
      coord_cartesian() + 
      scale_x_discrete() +
      scale_y_continuous() +
      labs(title="CO2 Emissions 2012") +
      labs(x=paste(""), y=paste("")) +
      theme(axis.text.x=element_text(angle=90,hjust=1,vjust=0.5))+
    
      layer(data=df1(), 
            mapping=aes(x=COUNTRY, y=as.numeric(CO2_EMISSIONS), color=COUNTRY, fill=COUNTRY), 
            stat="identity", 
            geom="bar",
            position=position_identity()
      )
    plot
  })
    
    observeEvent(input$clicks, {
      print(as.numeric(input$clicks))
    })
  
  #tab2
  output$Plot2 <- renderPlot({             
    plot <- ggplot() + 
      coord_cartesian() + 
      scale_x_continuous() +
      scale_y_continuous() +
      labs(title="CO2 Emissions for United States, Canada, France, and Germany") +
      labs(x=paste(""), y=paste("")) +
      layer(data=df1, 
            mapping=aes(x=as.numeric(YEAR), y=as.numeric(CO2_EMISSIONS), color=COUNTRY), 
            stat="identity", 
            geom="line",
            position=position_identity()
      )
    plot
  })
  
})