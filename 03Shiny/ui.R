#ui
require(shiny)
require(shinydashboard)
require(leaflet)

dashboardPage(
  
  dashboardHeader(title = 'Project 6'),
  
  dashboardSidebar(
    sidebarMenu(
      menuItem("Top Polluters", tabName = "plot1", icon = icon("th")),
      menuItem("plot2", tabName = "plot2", icon = icon("th")))
  ),
  
  dashboardBody(
    tabItems(
      
      #first tab
      tabItem(tabName = "plot1",
        sliderInput("maxval", "Minimum Emissions:", 
          min = 200, max = 1000,  value = 1000, step = 50),
        h2("Emissions For Different Nations Above Input Value"),
        actionButton(inputId = "clicks1",  label = "Generate Plot"),
        plotOutput("Plot1")
      ),
      
      #second tab
      tabItem(tabName = "plot2",
              h2("Recent Emissions For United States"),
              plotOutput("Plot2")
      )
    )
  )
)
